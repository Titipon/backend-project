package com.bezkoder.springjwt.security.services;

import java.util.List;

import com.bezkoder.springjwt.handler.ProcessException;
import com.bezkoder.springjwt.handler.ServiceResult;
import com.bezkoder.springjwt.models.CountChartModel;
import com.bezkoder.springjwt.models.CountRequestModel;
import com.bezkoder.springjwt.models.Customer;
import com.bezkoder.springjwt.models.IdRequest;
import com.bezkoder.springjwt.models.SearchStartEndDateModel;
import com.bezkoder.springjwt.models.Vehicle;

public interface CusAndVehService {
	
	public ServiceResult<Customer> saveCustomer(Customer param) throws ProcessException;
	
	public ServiceResult<Vehicle> saveVehicle(Vehicle param) throws ProcessException;

	public ServiceResult<List<Customer>> getCusAndVehList() throws ProcessException;
	
	public ServiceResult<List<Vehicle>> getCusAndVehByAccessdateBetween(SearchStartEndDateModel param) throws ProcessException;
	
	public ServiceResult<List<Vehicle>> getCusAndVehByExpiredate7Day() throws ProcessException;

	public ServiceResult<List<Vehicle>> getCusAndVehByExpiredate30Day() throws ProcessException;

	public ServiceResult<Customer> getCustomerByCusId(IdRequest param) throws ProcessException;

	public ServiceResult<List<Vehicle>> getVehicleList() throws ProcessException;

	public ServiceResult<CountChartModel> getCountByparam(CountRequestModel param) throws ProcessException;

	public ServiceResult<CountChartModel> getCountByTaxInsur(CountRequestModel param) throws ProcessException;

	public ServiceResult<List<Vehicle>> getVehicleListbyEnable() throws ProcessException;
	
//	public ServiceResult<List<VehicleYear>> getVehicleYearList() throws ProcessException;
//
//	public ServiceResult<VehicleYear> saveYear(VehicleYear savevehicleYear) throws ProcessException;
//	
}
