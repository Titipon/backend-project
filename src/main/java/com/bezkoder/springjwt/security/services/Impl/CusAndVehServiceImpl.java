package com.bezkoder.springjwt.security.services.Impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bezkoder.springjwt.handler.ProcessException;
import com.bezkoder.springjwt.handler.ServiceResult;
import com.bezkoder.springjwt.models.CountChartModel;
import com.bezkoder.springjwt.models.CountRequestModel;
import com.bezkoder.springjwt.models.Customer;
import com.bezkoder.springjwt.models.IdRequest;
import com.bezkoder.springjwt.models.SearchStartEndDateModel;
import com.bezkoder.springjwt.models.Vehicle;
import com.bezkoder.springjwt.repository.CustomerRepository;
import com.bezkoder.springjwt.repository.VehicleRepository;
import com.bezkoder.springjwt.security.services.CusAndVehService;

@Service
public class CusAndVehServiceImpl implements CusAndVehService{
	
//	@Autowired
//	private VehicleYearRepository vehicleYearRepository;
//	
	
	@Autowired
	private CustomerRepository customerRepository;
	
	@Autowired
	private VehicleRepository vehicleRepository;
	
	
	@Override
	public ServiceResult<Customer> saveCustomer(Customer param) throws ProcessException {
		ServiceResult<Customer> serviceResult = new ServiceResult<Customer>();
		Customer result = customerRepository.save(param);
		serviceResult.setData(result);
		serviceResult.setMessageCode("OK");
		serviceResult.setSuccess(true);
		return serviceResult;
	}

//	@Override
//	public ServiceResult<Customer> saveVehicle(Customer param) throws ProcessException {
//		ServiceResult<Customer> serviceResult = new ServiceResult<Customer>();
//		Customer result = customerRepository.save(param);
//		if (param.getVehicle().size() > 0) {
//			for (Vehicle obj : param.getVehicle()) {
//				obj.setCustomer(result);
//				vehicleRepository.save(obj);
//			}
//		}
//		serviceResult.setData(result);
//		serviceResult.setSuccess(true);
//		serviceResult.setMessageCode("OK");
//		return serviceResult;
//	}

	@Override
	public ServiceResult<Vehicle> saveVehicle(Vehicle param) throws ProcessException {
		ServiceResult<Vehicle> serviceResult = new ServiceResult<Vehicle>();
		Vehicle result = vehicleRepository.save(param);
		serviceResult.setData(result);
		serviceResult.setSuccess(true);
		serviceResult.setMessageCode("OK");
		return serviceResult;
	}

	@Override
	public ServiceResult<List<Customer>> getCusAndVehList() throws ProcessException {
		ServiceResult<List<Customer>> serviceResult = new ServiceResult<List<Customer>>();
		List<Customer> result = new ArrayList<Customer>();
		Iterable<Customer> iterable = customerRepository.findAll();
		if(iterable != null) {
			iterable.forEach(result::add);
		}
		serviceResult.setData(result);
		serviceResult.setSuccess(true);
		serviceResult.setMessageCode("OK");
		return serviceResult;
	}

//	@Override
//	public ServiceResult<List<Customer>> getCusAndVehByAccessdateBetween(SearchStartEndDateModel param)
//			throws ProcessException {
//		List<Customer> searchResult = new ArrayList<Customer>();
//		Iterable<Customer> iterable = customerRepository.findByAccessdateBetween(param.getStartDate(), param.getEndDate());
//		System.out.println("========================PrintDATA=========================");
//		iterable.forEach(System.out::println);
//		if(iterable != null) {
//			iterable.forEach(searchResult::add);
//		}
//		System.out.println("Startdate: "+param.getStartDate());
//		ServiceResult<List<Customer>> serviceResult = new ServiceResult<List<Customer>>();
//		serviceResult.setData(searchResult);
//		serviceResult.setSuccess(true);
//		serviceResult.setMessageCode("OK");
//		return serviceResult;
//	}
	@Override
	public ServiceResult<List<Vehicle>> getCusAndVehByAccessdateBetween(SearchStartEndDateModel param)
			throws ProcessException {
		List<Vehicle> searchResult = new ArrayList<Vehicle>();
		Iterable<Vehicle> iterable = vehicleRepository.findByAccessdateBetween(param.getStartDate(), param.getEndDate());
		if(iterable != null) {
			iterable.forEach(searchResult::add);
		}
		System.out.println("Startdate: "+param.getStartDate());
		ServiceResult<List<Vehicle>> serviceResult = new ServiceResult<List<Vehicle>>();
		serviceResult.setData(searchResult);
		serviceResult.setSuccess(true);
		serviceResult.setMessageCode("OK");
		return serviceResult;
	}

	@Override
	public ServiceResult<List<Vehicle>> getCusAndVehByExpiredate7Day()
			throws ProcessException {
		ServiceResult<List<Vehicle>> serviceResult = new ServiceResult<List<Vehicle>>();
		List<Vehicle> result = new ArrayList<Vehicle>();
		Iterable<Vehicle> iterable = vehicleRepository.findByExpireddate7Day();
		if(iterable != null) {
			iterable.forEach(result::add);
		}
		serviceResult.setData(result);
		serviceResult.setSuccess(true);
		serviceResult.setMessageCode("OK");
		return serviceResult;
	}
	
	@Override
	public ServiceResult<List<Vehicle>> getCusAndVehByExpiredate30Day()
			throws ProcessException {
		ServiceResult<List<Vehicle>> serviceResult = new ServiceResult<List<Vehicle>>();
		List<Vehicle> result = new ArrayList<Vehicle>();
		Iterable<Vehicle> iterable = vehicleRepository.findByExpireddate30Day();
		if(iterable != null) {
			iterable.forEach(result::add);
		}
		serviceResult.setData(result);
		serviceResult.setSuccess(true);
		serviceResult.setMessageCode("OK");
		return serviceResult;
	}

	@Override
	public ServiceResult<Customer> getCustomerByCusId(IdRequest param) throws ProcessException {
		ServiceResult<Customer> serviceResult = new ServiceResult<Customer>();
		Customer customer = customerRepository.findBycitizenId(param.getCitizenid());
		serviceResult.setData(customer);
		serviceResult.setSuccess(true);
		serviceResult.setMessageCode("OK");
		return serviceResult;
	}

//	@Override
//	public ServiceResult<VehicleDetail> saveVehDetail(VehicleDetail saveVehDetail) throws ProcessException {
//		ServiceResult<VehicleDetail> serviceResult = new ServiceResult<VehicleDetail>();
//		VehicleDetail result = vehicleDetailRepository.save(saveVehDetail);
//		serviceResult.setData(result);
//		serviceResult.setSuccess(true);
//		serviceResult.setMessageCode("OK");
//		return serviceResult;
//	}
//
//	@Override
//	public ServiceResult<List<VehicleDetail>> getAllVehDetail() throws ProcessException {
//		ServiceResult<List<VehicleDetail>> serviceResult = new ServiceResult<List<VehicleDetail>>();
//		List<VehicleDetail> result = new ArrayList<VehicleDetail>();
//		Iterable<VehicleDetail> iterable = vehicleDetailRepository.findAllVd();
//		if(iterable != null) {
//			iterable.forEach(result::add);
//		}
//		serviceResult.setData(result);
//		serviceResult.setSuccess(true);
//		serviceResult.setMessageCode("OK");
//		return serviceResult;
//	}
//	
	@Override
	public ServiceResult<List<Vehicle>> getVehicleList() throws ProcessException {
		ServiceResult<List<Vehicle>> serviceResult = new ServiceResult<List<Vehicle>>();
		List<Vehicle> result = new ArrayList<Vehicle>();
		Iterable<Vehicle> iterable = vehicleRepository.findAll();
		if(iterable != null) {
			iterable.forEach(result::add);
		}
		serviceResult.setData(result);
		serviceResult.setSuccess(true);
		serviceResult.setMessageCode("OK");
		return serviceResult;
	}
//	@Override
//	public ServiceResult<SpecDetail> saveSpecDetail(SpecDetail param) throws ProcessException {
//		ServiceResult<SpecDetail> serviceResult = new ServiceResult<SpecDetail>();
//		SpecDetail result = specDetailRepository.save(param);
//		serviceResult.setData(result);
//		serviceResult.setSuccess(true);
//		serviceResult.setMessageCode("OK");
//		return serviceResult;
//	}
//	@Override
//	public ServiceResult<List<SpecDetail>> getSpecList() throws ProcessException {
//		ServiceResult<List<SpecDetail>> serviceResult = new ServiceResult<List<SpecDetail>>();
//		List<SpecDetail> result = new ArrayList<SpecDetail>();
//		Iterable<SpecDetail> iterable = specDetailRepository.findAllVs();
//		if(iterable != null) {
//			iterable.forEach(result::add);
//		}
//		serviceResult.setData(result);
//		serviceResult.setSuccess(true);
//		serviceResult.setMessageCode("OK");
//		return serviceResult;
//	}
//	@Override
//	public ServiceResult<List<VehicleYear>> getVehicleYearList() throws ProcessException {
//		ServiceResult<List<VehicleYear>> serviceResult = new ServiceResult<List<VehicleYear>>();
//		List<VehicleYear> result = new ArrayList<VehicleYear>();
//		Iterable<VehicleYear> iterable = vehicleYearRepository.findAllVy();
//		if(iterable != null) {
//			iterable.forEach(result::add);
//		}
//		serviceResult.setData(result);
//		serviceResult.setSuccess(true);
//		serviceResult.setMessageCode("OK");
//		return serviceResult;
//	}
//	@Override
//	public ServiceResult<VehicleYear> saveYear(VehicleYear param) throws ProcessException {
//		ServiceResult<VehicleYear> serviceResult = new ServiceResult<VehicleYear>();
//		VehicleYear result = vehicleYearRepository.save(param);
//		serviceResult.setData(result);
//		serviceResult.setSuccess(true);
//		serviceResult.setMessageCode("OK");
//		return serviceResult;
//	}
//	

	@Override
	public ServiceResult<CountChartModel> getCountByparam(CountRequestModel param) throws ProcessException {
		ServiceResult<CountChartModel> serviceResult = new ServiceResult<CountChartModel>();
		CountChartModel countmodel = new CountChartModel();
		long count = vehicleRepository.countByVehicleType(param.getParam());
		countmodel.setCount(count);
		countmodel.setName(param.getRequestname());
		serviceResult.setData(countmodel);
		serviceResult.setSuccess(true);
		serviceResult.setMessageCode("OK");
		return serviceResult;
	}
	@Override
	public ServiceResult<CountChartModel> getCountByTaxInsur(CountRequestModel param) throws ProcessException {
		ServiceResult<CountChartModel> serviceResult = new ServiceResult<CountChartModel>();
		CountChartModel countmodel = new CountChartModel();
		long count = vehicleRepository.countByServiceType(param.getParam());
		countmodel.setCount(count);
		countmodel.setName(param.getRequestname());
		serviceResult.setData(countmodel);
		serviceResult.setSuccess(true);
		serviceResult.setMessageCode("OK");
		return serviceResult;
	}
	@Override
	public ServiceResult<List<Vehicle>> getVehicleListbyEnable() throws ProcessException {
		ServiceResult<List<Vehicle>> serviceResult = new ServiceResult<List<Vehicle>>();
		List<Vehicle> result = new ArrayList<Vehicle>();
		Iterable<Vehicle> iterable = vehicleRepository.findAllbystatus();
		if(iterable != null) {
			iterable.forEach(result::add);
		}
		serviceResult.setData(result);
		serviceResult.setSuccess(true);
		serviceResult.setMessageCode("OK");
		return serviceResult;
	}
}
