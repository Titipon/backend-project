package com.bezkoder.springjwt.handler;

import java.io.Serializable;

public class ServiceResult <T extends Object> implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    private T data;

    private boolean success;

    private String messageCode;
    
	public ServiceResult(T data) {
		super();
		this.data = data;
		this.success = false;
	}

	public ServiceResult() {
		
	}

	public T getData() {
		return data;
	}

	public void setData(T data) {
		this.data = data;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getMessageCode() {
		return messageCode;
	}

	public void setMessageCode(String messageCode) {
		this.messageCode = messageCode;
	}
    
    
}
