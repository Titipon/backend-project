package com.bezkoder.springjwt.handler;


public class ProcessException extends Exception{
	
	private String messageCode;
	
	private Throwable throwable;
	
	 public ProcessException(String messageCode) {
			this(messageCode, null);
		    }

		    public ProcessException(String messageCode, Throwable throwable) {
			this.messageCode = messageCode;
			this.throwable = throwable;
		    }

		    public String getMessageCode() {
			return messageCode;
		    }

		    public Throwable getThrowable() {
			return throwable;
		    }

}
