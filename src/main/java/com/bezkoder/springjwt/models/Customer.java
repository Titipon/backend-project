package com.bezkoder.springjwt.models;

import java.io.Serializable;
import java.sql.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="Customer")
@NamedQuery(name="Customer.findAll", query="SELECT c FROM Customer c")
public class Customer implements Serializable{
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="cus_id", unique=true, nullable=false )
	private Integer cusId;
	
	@Column(name="prefixname", nullable = false,length = 20)
	private String prefixname;
	
	@Column(name="name", nullable = false, length = 30)
	private String name;
	
	@Column(name="surname", nullable = false , length = 50)
	private String surname;
	
	@Column(name="citizenid", nullable = false , length = 13)
	private String citizenId;
	
	@Column(name="telno", nullable = false , length = 10)
	private String telno;
	
	@Column(name="address" ,nullable = false )
	private String address;
	
	@Column(name="subdistrict" ,nullable = false )
	private String subdistrict;
	
	@Column(name="district" ,nullable = false )
	private String district;
	
	@Column(name="province" ,nullable = false )
	private String province;
	
	@Column(name="createby", nullable = false , length = 15)
	private String createby;
	
	@Column(name="createdate", nullable = false)
	private Date createdate;

	@OneToMany(mappedBy="customer",cascade = CascadeType.REMOVE)
	private List<Vehicle> vehicle;
	
	

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getSubdistrict() {
		return subdistrict;
	}

	public void setSubdistrict(String subdistrict) {
		this.subdistrict = subdistrict;
	}

	public String getDistrict() {
		return district;
	}

	public void setDistrict(String district) {
		this.district = district;
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	public Integer getCusId() {
		return cusId;
	}

	public void setCusId(Integer cusId) {
		this.cusId = cusId;
	}

	public String getPrefixname() {
		return prefixname;
	}

	public void setPrefixname(String prefixname) {
		this.prefixname = prefixname;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getCitizenId() {
		return citizenId;
	}

	public void setCitizenId(String citizenId) {
		this.citizenId = citizenId;
	}

	public String getTelno() {
		return telno;
	}

	public void setTelno(String telno) {
		this.telno = telno;
	}

	public String getCreateby() {
		return createby;
	}

	public void setCreateby(String createby) {
		this.createby = createby;
	}

	public Date getCreatedate() {
		return createdate;
	}

	public void setCreatedate(Date currentDate) {
		this.createdate = currentDate;
	}

	public List<Vehicle> getVehicle() {
		return vehicle;
	}

	public void setVehicle(List<Vehicle> vehicle) {
		this.vehicle = vehicle;
	}
//	public Vehicle addVehicle(Vehicle vehicle) {
//		getVehicle().add(vehicle);
//		vehicle.setCustomer(this);
//
//		return vehicle;
//	}
//	public Vehicle removeVehicle(Vehicle vehicle) {
//		getVehicle().remove(vehicle);
//		vehicle.setCustomer(this);
//
//		return vehicle;
//	}
	
}
