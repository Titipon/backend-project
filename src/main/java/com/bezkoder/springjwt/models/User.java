package com.bezkoder.springjwt.models;

import java.sql.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table(	name = "users", 
		uniqueConstraints = { 
			@UniqueConstraint(columnNames = "username"),
			@UniqueConstraint(columnNames = "email") 
		})
public class User {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@NotBlank
	@Size(max = 20)
	private String username;

	@NotBlank
	@Size(max = 50)
	@Email
	private String email;

	@NotBlank
	@Size(max = 120)
	private String password;

	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(	name = "user_roles", 
				joinColumns = @JoinColumn(name = "user_id"), 
				inverseJoinColumns = @JoinColumn(name = "role_id"))
	private Set<Role> roles = new HashSet<>();
	
	@NotBlank
	@Size(max = 20)
	private String prefixname;
	
	@NotBlank
	@Size(max = 30)
	private String name;
	
	@NotBlank
	@Size(max = 50)
	private String surname;
	
	@Size(max = 20)
	private String createby;
	
	@JsonFormat(pattern="dd-MM-yyyy")
	private Date createdate;


	public User() {
	}


//public User (@NotBlank @Size(max = 20) String username, @NotBlank @Size(max = 50) @Email String email,
//			@NotBlank @Size(max = 120) String password, Set<Role> roles, @NotBlank @Size(max = 20) String prefixname,
//			@NotBlank @Size(max = 30) String name, @NotBlank @Size(max = 50) String surname,
//			@Size(max = 20) String createby, Date createdate) {
//		super();
//		this.id = id;
//		this.username = username;
//		this.email = email;
//		this.password = password;
//		this.roles = roles;
//		this.prefixname = prefixname;
//		this.name = name;
//		this.surname = surname;
//		this.createby = createby;
//		this.createdate = createdate;
//	}


	public User(Integer id,@NotBlank @Size(max = 20) String username, @NotBlank @Size(max = 50) @Email String email,
			@NotBlank @Size(max = 120) String password, @NotBlank @Size(max = 20) String prefixname,
			@NotBlank @Size(max = 30) String name, @NotBlank @Size(max = 50) String surname,
			@Size(max = 20) String createby,Date createdate) {
		super();
		this.id = id;
		this.username = username;
		this.email = email;
		this.password = password;
		this.prefixname = prefixname;
		this.name = name;
		this.surname = surname;
		this.createby = createby;
		this.createdate = createdate;
	}


	public Integer getId() {
		return id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public String getUsername() {
		return username;
	}


	public void setUsername(String username) {
		this.username = username;
	}


	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}


	public String getPassword() {
		return password;
	}


	public void setPassword(String password) {
		this.password = password;
	}


	public Set<Role> getRoles() {
		return roles;
	}


	public void setRoles(Set<Role> roles) {
		this.roles = roles;
	}


	public String getPrefixname() {
		return prefixname;
	}


	public void setPrefixname(String prefixname) {
		this.prefixname = prefixname;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getSurname() {
		return surname;
	}


	public void setSurname(String surname) {
		this.surname = surname;
	}


	public String getCreateby() {
		return createby;
	}


	public void setCreateby(String createby) {
		this.createby = createby;
	}


	public Date getCreatedate() {
		return createdate;
	}


	public void setCreatedate(Date createdate) {
		this.createdate = createdate;
	}
	

//	public User(String username, String email, String password) {
//		this.username = username;
//		this.email = email;
//		this.password = password;
//	}
//
//	public Long getId() {
//		return id;
//	}
//
//	public void setId(Long id) {
//		this.id = id;
//	}
//
//	public String getUsername() {
//		return username;
//	}
//
//	public void setUsername(String username) {
//		this.username = username;
//	}
//
//	public String getEmail() {
//		return email;
//	}
//
//	public void setEmail(String email) {
//		this.email = email;
//	}
//
//	public String getPassword() {
//		return password;
//	}
//
//	public void setPassword(String password) {
//		this.password = password;
//	}
//
//	public Set<Role> getRoles() {
//		return roles;
//	}
//
//	public void setRoles(Set<Role> roles) {
//		this.roles = roles;
//	}
}
