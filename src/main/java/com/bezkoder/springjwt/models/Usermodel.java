package com.bezkoder.springjwt.models;
import java.sql.Date;
import java.util.Set;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonFormat;

public class Usermodel {
	
		
		
		
		private Integer id;
	    @NotBlank
	    @Size(min = 3, max = 20)
	    private String username;
	 
	    @NotBlank
	    @Size(max = 50)
	    @Email
	    private String email;
	    
	    private Set<Role> role;
	    
	    @NotBlank
	    @Size(min = 6, max = 40)
	    private String password;
	    
	    @NotBlank
		@Size(max = 20)
		private String prefixname;
		
		@NotBlank
		@Size(max = 30)
		private String name;
		
		@NotBlank
		@Size(max = 50)
		private String surname;
		
		@Size(max = 20)
		private String createby;
		
		@JsonFormat(pattern = "dd-MM-yyyy HH:mm:ss",timezone = "Asia/Bangkok",locale = "th: Thai")
		private Date createdate;
		

		public Usermodel(User user) {
			this.id = user.getId();
			this.username = user.getUsername();
			this.email = user.getEmail();
			this.password = user.getPassword();
			this.role = user.getRoles();
			this.prefixname = user.getPrefixname();
			this.name = user.getName();
			this.surname = user.getSurname();
		}

		public Integer getId() {
			return id;
		}

		public void setId(Integer id) {
			this.id = id;
		}

		public String getCreateby() {
			return createby;
		}

		public void setCreateby(String createby) {
			this.createby = createby;
		}

		public Date getCreatedate() {
			return createdate;
		}

		public void setCreatedate(Date createdate) {
			this.createdate = createdate;
		}

		public String getUsername() {
			return username;
		}

		public void setUsername(String username) {
			this.username = username;
		}

		public String getEmail() {
			return email;
		}

		public void setEmail(String email) {
			this.email = email;
		}

		public Set<Role> getRole() {
			return role;
		}

		public void setRole(Set<Role> role) {
			this.role = role;
		}

		public String getPassword() {
			return password;
		}

		public void setPassword(String password) {
			this.password = password;
		}

		public String getPrefixname() {
			return prefixname;
		}

		public void setPrefixname(String prefixname) {
			this.prefixname = prefixname;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public String getSurname() {
			return surname;
		}

		public void setSurname(String surname) {
			this.surname = surname;
		}
		
	  
}


