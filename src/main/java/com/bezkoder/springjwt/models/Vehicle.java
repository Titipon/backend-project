package com.bezkoder.springjwt.models;

import java.io.Serializable;
import java.sql.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="Vehicle")
@NamedQuery(name="Vehicle.findAll", query="SELECT v FROM Vehicle v")
public class Vehicle implements Serializable{
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="vehicle_id", unique=true, nullable=false)
	private Integer vehicleId;
	
	@Column(name="accessdate"  , nullable = false)
	private Date accessdate;
	
	@Column(name="vehicle_reg"  , nullable = false , length = 10)
	private String vehicleReg;
	
	@Column(name="province"  , nullable = false , length = 30)
	private String province;
	
	@Column(name="chassis_no"  , nullable = false , length = 30)
	private String chassisNo;
	
	@Column(name="start_date" , nullable = false)
	private Date startdate;
	
	@Column(name="expire_date"  , nullable = false)
	private Date expiredate;
	
	@Column(name="servicetype" , nullable = false , length = 20)
	private String serviceType;
	
	@Column(name="vehicletype" , nullable = false , length = 20)
	private String vehicleType;
	
	@Column(name="receivetype" , nullable = false , length = 20)
	private String receivetype;
	
	@Column(name="policy_no", nullable = true , length = 20)
	private String policyNo;
	
	@Column(name="insuranceType" , nullable = true , length = 50)
	private String insuranceType;
	
	@Column(name="insuranceCompany" , nullable = true , length = 50)
	private String insuranceCompany;
	
	@Column(name="vehicleACTfee" , nullable = true , length = 5)
	private Integer vehicleACTfee;
	
	@Column(name="servicefee" , nullable = true , length = 5)
	private Integer servicefee;
	
	@Column(name="vehicletax", nullable = true , length = 5)
	private Integer vehicletax;
	
	@Column(name="vehiclebrand", nullable = false)
	private String vehiclebrand;
	
	@Column(name="vehiclemodel", nullable = false)
	private String vehiclemodel;
	
	@Column(name="premiumfee" , nullable = true , length = 5)
	private Integer premiumfee;

	@Column(name="createby", nullable = false , length = 15)
	private String createby;
	
	@Column(name="createdate", nullable = false)
	private Date createdate;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cus_id", nullable=false ,insertable = true)
	private Customer customer;
	

	@Column(name="vehicleYear" ,nullable = false)
	private String vehicleyear;
	
	@Column(name="status" ,nullable = false)
	private String status;
	

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getVehicleyear() {
		return vehicleyear;
	}

	public String getVehiclebrand() {
		return vehiclebrand;
	}

	public void setVehiclebrand(String vehiclebrand) {
		this.vehiclebrand = vehiclebrand;
	}

	public String getVehiclemodel() {
		return vehiclemodel;
	}

	public void setVehiclemodel(String vehiclemodel) {
		this.vehiclemodel = vehiclemodel;
	}

	public void setVehicleyear(String vehicleyear) {
		this.vehicleyear = vehicleyear;
	}


	public String getVehicleType() {
		return vehicleType;
	}

	public void setVehicleType(String vehicleType) {
		this.vehicleType = vehicleType;
	}

	public String getReceivetype() {
		return receivetype;
	}

	public void setReceivetype(String receivetype) {
		this.receivetype = receivetype;
	}
	
	public Vehicle() {
	}

	public Integer getVehicleId() {
		return vehicleId;
	}

	public void setVehicleId(Integer vehicleId) {
		this.vehicleId = vehicleId;
	}

	public Date getAccessdate() {
		return accessdate;
	}

	public void setAccessdate(Date accessdate) {
		this.accessdate = accessdate;
	}

	public String getVehicleReg() {
		return vehicleReg;
	}

	public void setVehicleReg(String vehicleReg) {
		this.vehicleReg = vehicleReg;
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	public String getChassisNo() {
		return chassisNo;
	}

	public void setChassisNo(String chassisNo) {
		this.chassisNo = chassisNo;
	}

	public Date getStartdate() {
		return startdate;
	}

	public void setStartdate(Date startdate) {
		this.startdate = startdate;
	}

	public Date getExpiredate() {
		return expiredate;
	}

	public void setExpiredate(Date expiredate) {
		this.expiredate = expiredate;
	}

	public String getServiceType() {
		return serviceType;
	}

	public void setServiceType(String serviceType) {
		this.serviceType = serviceType;
	}

	public String getPolicyNo() {
		return policyNo;
	}

	public void setPolicyNo(String policyNo) {
		this.policyNo = policyNo;
	}

	public String getInsuranceType() {
		return insuranceType;
	}

	public void setInsuranceType(String insuranceType) {
		this.insuranceType = insuranceType;
	}

	public String getInsuranceCompany() {
		return insuranceCompany;
	}

	public void setInsuranceCompany(String insuranceCompany) {
		this.insuranceCompany = insuranceCompany;
	}

	public Integer getVehicleACTfee() {
		return vehicleACTfee;
	}

	public void setVehicleACTfee(Integer vehicleACTfee) {
		this.vehicleACTfee = vehicleACTfee;
	}

	public Integer getServicefee() {
		return servicefee;
	}

	public void setServicefee(Integer servicefee) {
		this.servicefee = servicefee;
	}

	public Integer getVehicletax() {
		return vehicletax;
	}

	public void setVehicletax(Integer vehicletax) {
		this.vehicletax = vehicletax;
	}

	public Integer getPremiumfee() {
		return premiumfee;
	}

	public void setPremiumfee(Integer premiumfee) {
		this.premiumfee = premiumfee;
	}

	public String getCreateby() {
		return createby;
	}

	public void setCreateby(String createby) {
		this.createby = createby;
	}

	public Date getCreatedate() {
		return createdate;
	}

	public void setCreatedate(Date createdate) {
		this.createdate = createdate;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}
	
	
}
