package com.bezkoder.springjwt.models;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(Include.NON_NULL)
public class CustomerModel{

	@JsonProperty(value="cus_id")
	private Integer cusId;
	
	@JsonProperty(value="prefixname")
	private String prefixname;
	
	@JsonProperty(value="name")
	private String name;
	
	@JsonProperty(value="surname")
	private String surname;
	
	@JsonProperty(value="citizenid")
	private String citizenId;
	
	@JsonProperty(value="telno")
	private String telno;
	
	@JsonProperty("address")
	private String address;
	
	@JsonProperty("subdistrict")
	private String subdistrict;
	
	@JsonProperty("district" )
	private String district;
	
	@JsonProperty("province")
	private String province;
	
	@JsonProperty("fullname")
	private String fullname;
	
	@JsonProperty(value="createby")
	private String createby;
	
	@Temporal(TemporalType.DATE)
	@JsonFormat(pattern = "dd/MM/yyyy",timezone = "Asia/Bangkok",locale = "th: Thai")
	@JsonProperty(value="createdate")
	private Date createdate;
	
	@JsonProperty(value="vehicle")
	private List<VehicleModel> vehicle;
	
	

	public String getFullname() {
		return fullname;
	}

	public void setFullname(String fullname) {
		this.fullname = fullname;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getSubdistrict() {
		return subdistrict;
	}

	public void setSubdistrict(String subdistrict) {
		this.subdistrict = subdistrict;
	}

	public String getDistrict() {
		return district;
	}

	public void setDistrict(String district) {
		this.district = district;
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	public Integer getCusId() {
		return cusId;
	}

	public void setCusId(Integer cusId) {
		this.cusId = cusId;
	}

	public String getPrefixname() {
		return prefixname;
	}

	public void setPrefixname(String prefixname) {
		this.prefixname = prefixname;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getCitizenId() {
		return citizenId;
	}

	public void setCitizenId(String citizenId) {
		this.citizenId = citizenId;
	}

	public String getTelno() {
		return telno;
	}

	public void setTelno(String telno) {
		this.telno = telno;
	}

	public String getCreateby() {
		return createby;
	}

	public void setCreateby(String createby) {
		this.createby = createby;
	}

	public Date getCreatedate() {
		return createdate;
	}

	public void setCreatedate(Date createdate) {
		this.createdate = createdate;
	}

	public List<VehicleModel> getVehicle() {
		return vehicle;
	}

	public void setVehicle(List<VehicleModel> vehicle) {
		this.vehicle = vehicle;
	}

	public CustomerModel() {
		super();
	}

	public CustomerModel(Customer entity) {
		super();
		this.cusId = entity.getCusId();
		this.prefixname = entity.getPrefixname();
		this.name = entity.getName();
		this.surname = entity.getSurname();
		this.citizenId = entity.getCitizenId();
		this.telno = entity.getTelno();
		this.createby = entity.getCreateby();
		this.createdate = entity.getCreatedate();
		this.address = entity.getAddress();
		this.subdistrict = entity.getSubdistrict();
		this.district = entity.getDistrict();
		this.province = entity.getProvince();
		this.fullname = entity.getPrefixname()  + entity.getName() + " " +entity.getSurname();  
		List<VehicleModel> memList = new ArrayList<VehicleModel>();
		if(entity.getVehicle()!=null) {
			for(Vehicle mem :entity.getVehicle()) {
				VehicleModel memModel = new VehicleModel(mem);
				memList.add(memModel);
			}
		}
		this.vehicle = memList;
	}
	
	
}
