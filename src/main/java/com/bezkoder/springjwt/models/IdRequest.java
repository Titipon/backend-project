package com.bezkoder.springjwt.models;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

public class IdRequest implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@JsonProperty("id")
	private long id;
	
	@JsonProperty("intid")
	private Integer intId;
	
	@JsonProperty("citizenid")
	private String citizenid;

	
	
	public String getCitizenid() {
		return citizenid;
	}

	public void setCitizenid(String citizenid) {
		this.citizenid = citizenid;
	}

	public Integer getIntId() {
		return intId;
	}

	public void setIntId(Integer intId) {
		this.intId = intId;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public IdRequest() {
		super();
	}
	
	

}
