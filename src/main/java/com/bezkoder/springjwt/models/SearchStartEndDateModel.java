package com.bezkoder.springjwt.models;

import java.io.Serializable;
import java.sql.Date;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public class SearchStartEndDateModel implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Temporal(TemporalType.DATE)
	@JsonFormat(pattern = "dd/MM/yyyy",timezone = "Asia/Bangkok",locale = "th: Thai")
	@JsonProperty(value="startdate")
	private Date startDate;
	
	@Temporal(TemporalType.DATE)
	@JsonFormat(pattern = "dd/MM/yyyy",timezone = "Asia/Bangkok",locale = "th: Thai")
	@JsonProperty(value="enddate")
	private Date endDate;

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public SearchStartEndDateModel() {
		super();
	}

	public SearchStartEndDateModel(Date startDate, Date endDate) {
		super();
		this.startDate = startDate;
		this.endDate = endDate;
	}

	
	
	

}
