package com.bezkoder.springjwt.models;

import java.io.Serializable;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.bezkoder.springjwt.Utils.DateUtils;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(Include.NON_NULL)
public class VehicleModel implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@JsonProperty(value="vehicle_id")
	private Integer vehicleId;
	
	@Temporal(TemporalType.DATE)
	@JsonFormat(pattern = "dd/MM/yyyy",timezone = "Asia/Bangkok",locale = "th: Thai")
	@JsonProperty(value="accessdate")
	private Date accessdate;
	
	@JsonProperty(value="vehicle_reg")
	private String vehicleReg;
	
	@JsonProperty(value="province")
	private String province;
	
	@JsonProperty(value="chassis_no")
	private String chassisNo;
	
	@Temporal(TemporalType.DATE)
	@JsonFormat(pattern = "dd/MM/yyyy",timezone = "Asia/Bangkok",locale = "th: Thai")
	@JsonProperty(value="start_date")
	private Date startdate;
	
	@Temporal(TemporalType.DATE)
	@JsonFormat(pattern = "dd/MM/yyyy",timezone = "Asia/Bangkok",locale = "th: Thai")
	@JsonProperty(value="expire_date")
	private Date expiredate;
	
	@JsonProperty(value = "restDate")
	private String restDate;
	
	@JsonProperty(value="servicetype")
	private String serviceType;
	
	@JsonProperty(value="vehicletype")
	private String vehicletype;
	
	@JsonProperty(value="receivetype")
	private String receivetype;
	
	@JsonProperty(value="policy_no")
	private String policyNo;
	
	@JsonProperty(value="insuranceType")
	private String insuranceType;
	
	@JsonProperty(value="insuranceCompany")
	private String insuranceCompany;
	
	@JsonProperty(value="vehicleACTfee")
	private Integer vehicleACTfee;
	
	@JsonProperty(value="servicefee")
	private Integer servicefee;
	
	@JsonProperty(value="vehicletax")
	private Integer vehicletax;
	
	@JsonProperty(value="premiumfee")
	private Integer premiumfee;
	
	@JsonProperty(value="status")
	private String status;

	@JsonProperty(value="createby")
	private String createby;
	
	@Temporal(TemporalType.DATE)
	@JsonFormat(pattern = "dd/MM/yyyy",timezone = "Asia/Bangkok",locale = "th: Thai")
	@JsonProperty(value="createdate")
	private Date createdate;
	
	@JsonProperty(value="cus_id")
	private Integer cusId;
	
	@JsonProperty(value="cus_citizenId")
	private String cusCitizenId;
	
	@JsonProperty(value="cus_prefixname")
	private String cusPrefixname;
	
	@JsonProperty(value="cus_name")
	private String cusName;
	
	@JsonProperty(value="cus_surname")
	private String cusSurname;
	
	@JsonProperty(value="cus_telNo")
	private String cusTelNo;
	
	@JsonProperty(value="vehicleYear")
	private String vehicleyear;
	
	@JsonProperty(value="vehiclebrand")
	private String vehiclebrand;
	
	@JsonProperty(value="vehiclemodel")
	private String vehiclemodel;
	
	@JsonProperty(value="cus_address")
	private String cusaddress;
	
	@JsonProperty(value="cus_fullname")
	private String cusfullname;
	
	@JsonProperty(value="fullreg")
	private String fullreg;
	
	
	
	
	
	
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getFullreg() {
		return fullreg;
	}

	public void setFullreg(String fullreg) {
		this.fullreg = fullreg;
	}

	public String getCusfullname() {
		return cusfullname;
	}

	public void setCusfullname(String cusfullname) {
		this.cusfullname = cusfullname;
	}

	public String getCusaddress() {
		return cusaddress;
	}

	public void setCusaddress(String cusaddress) {
		this.cusaddress = cusaddress;
	}

	public String getVehiclebrand() {
		return vehiclebrand;
	}

	public void setVehiclebrand(String vehiclebrand) {
		this.vehiclebrand = vehiclebrand;
	}

	public String getVehiclemodel() {
		return vehiclemodel;
	}

	public void setVehiclemodel(String vehiclemodel) {
		this.vehiclemodel = vehiclemodel;
	}

	public String getVehicleyear() {
		return vehicleyear;
	}

	public void setVehicleyear(String vehicleyear) {
		this.vehicleyear = vehicleyear;
	}

	public String getVehicletype() {
		return vehicletype;
	}

	public void setVehicletype(String vehicletype) {
		this.vehicletype = vehicletype;
	}

	public String getReceivetype() {
		return receivetype;
	}

	public void setReceivetype(String receivetype) {
		this.receivetype = receivetype;
	}

	public String getRestDate() {
		return restDate;
	}

	public void setRestDate(String restDate) {
		this.restDate = restDate;
	}

	public Integer getCusId() {
		return cusId;
	}

	public void setCusId(Integer cusId) {
		this.cusId = cusId;
	}

	public String getCusCitizenId() {
		return cusCitizenId;
	}

	public void setCusCitizenId(String cusCitizenId) {
		this.cusCitizenId = cusCitizenId;
	}

	public String getCusPrefixname() {
		return cusPrefixname;
	}

	public void setCusPrefixname(String cusPrefixname) {
		this.cusPrefixname = cusPrefixname;
	}

	public String getCusName() {
		return cusName;
	}

	public void setCusName(String cusName) {
		this.cusName = cusName;
	}

	public String getCusSurname() {
		return cusSurname;
	}

	public void setCusSurname(String cusSurname) {
		this.cusSurname = cusSurname;
	}

	public String getCusTelNo() {
		return cusTelNo;
	}

	public void setCusTelNo(String cusTelNo) {
		this.cusTelNo = cusTelNo;
	}

	public Integer getVehicleId() {
		return vehicleId;
	}

	public void setVehicleId(Integer vehicleId) {
		this.vehicleId = vehicleId;
	}

	public Date getAccessdate() {
		return accessdate;
	}

	public void setAccessdate(Date accessdate) {
		this.accessdate = accessdate;
	}

	public String getVehicleReg() {
		return vehicleReg;
	}

	public void setVehicleReg(String vehicleReg) {
		this.vehicleReg = vehicleReg;
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	public String getChassisNo() {
		return chassisNo;
	}

	public void setChassisNo(String chassisNo) {
		this.chassisNo = chassisNo;
	}

	public Date getStartdate() {
		return startdate;
	}

	public void setStartdate(Date startdate) {
		this.startdate = startdate;
	}

	public Date getExpiredate() {
		return expiredate;
	}

	public void setExpiredate(Date expiredate) {
		this.expiredate = expiredate;
	}

	public String getServiceType() {
		return serviceType;
	}

	public void setServiceType(String serviceType) {
		this.serviceType = serviceType;
	}

	public String getPolicyNo() {
		return policyNo;
	}

	public void setPolicyNo(String policyNo) {
		this.policyNo = policyNo;
	}

	public String getInsuranceType() {
		return insuranceType;
	}

	public void setInsuranceType(String insuranceType) {
		this.insuranceType = insuranceType;
	}

	public String getInsuranceCompany() {
		return insuranceCompany;
	}

	public void setInsuranceCompany(String insuranceCompany) {
		this.insuranceCompany = insuranceCompany;
	}

	public Integer getVehicleACTfee() {
		return vehicleACTfee;
	}

	public void setVehicleACTfee(Integer vehicleACTfee) {
		this.vehicleACTfee = vehicleACTfee;
	}

	public Integer getServicefee() {
		return servicefee;
	}

	public void setServicefee(Integer servicefee) {
		this.servicefee = servicefee;
	}

	public Integer getVehicletax() {
		return vehicletax;
	}

	public void setVehicletax(Integer vehicletax) {
		this.vehicletax = vehicletax;
	}

	public Integer getPremiumfee() {
		return premiumfee;
	}

	public void setPremiumfee(Integer premiumfee) {
		this.premiumfee = premiumfee;
	}

	public String getCreateby() {
		return createby;
	}

	public void setCreateby(String createby) {
		this.createby = createby;
	}

	public Date getCreatedate() {
		return createdate;
	}

	public void setCreatedate(Date createdate) {
		this.createdate = createdate;
	}

	public Integer getCusid() {
		return cusId;
	}

	public void setCusid(Integer cusid) {
		this.cusId = cusid;
	}
	


//	public Integer getVy_id() {
//		return vy_id;
//	}
//
//	public void setVy_id(Integer vy_id) {
//		this.vy_id = vy_id;
//	}


	public VehicleModel(Vehicle entity) {
		this.vehicleId = entity.getVehicleId();
		this.accessdate = entity.getAccessdate();
		this.vehicleReg = entity.getVehicleReg();
		this.province = entity.getProvince();
		this.chassisNo = entity.getChassisNo();
		this.startdate = entity.getStartdate();
		this.expiredate = entity.getExpiredate();
		this.serviceType = entity.getServiceType();
		this.policyNo = entity.getPolicyNo();
		this.insuranceType = entity.getInsuranceType();
		this.insuranceCompany = entity.getInsuranceCompany();
		this.vehicleACTfee = entity.getVehicleACTfee();
		this.servicefee = entity.getServicefee();
		this.vehicletax = entity.getVehicletax();
		this.premiumfee = entity.getPremiumfee();
		this.createby = entity.getCreateby();
		this.createdate = entity.getCreatedate();
		this.receivetype = entity.getReceivetype();
		this.vehicletype = entity.getVehicleType();
		this.vehicleyear = entity.getVehicleyear();
		this.vehiclebrand = entity.getVehiclebrand();
		this.vehiclemodel = entity.getVehiclemodel();
		this.status = entity.getStatus();
		this.fullreg = entity.getVehicleReg().concat(" "+entity.getProvince());
		if(entity.getCustomer() != null) {
			this.cusId = entity.getCustomer().getCusId();
			this.cusPrefixname = entity.getCustomer().getPrefixname();
			this.cusName = entity.getCustomer().getName();
			this.cusSurname = entity.getCustomer().getSurname();
			this.cusCitizenId = entity.getCustomer().getCitizenId();
			this.cusTelNo = entity.getCustomer().getTelno();
			this.restDate = DateUtils.getRestDate(entity.getExpiredate());
			if(entity.getCustomer().getAddress() != null && entity.getCustomer().getSubdistrict()!=null && entity.getCustomer().getDistrict() !=null && entity.getCustomer().getProvince() !=null
					&& entity.getCustomer().getPrefixname() !=null && entity.getCustomer().getName() !=null && entity.getCustomer().getSurname()!=null) {
			this.cusaddress = entity.getCustomer().getAddress().concat(" "+entity.getCustomer().getSubdistrict().concat(" "+entity.getCustomer().getDistrict().concat(" "+ entity.getCustomer().getProvince())));
			this.cusfullname = entity.getCustomer().getPrefixname().concat(entity.getCustomer().getName().concat(" "+entity.getCustomer().getSurname()));
			}
		}
	}

	public VehicleModel() {

}
	
	
}
