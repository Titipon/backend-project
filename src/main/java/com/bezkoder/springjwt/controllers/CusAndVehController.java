package com.bezkoder.springjwt.controllers;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bezkoder.springjwt.BAO.CusAndVehBAO;
import com.bezkoder.springjwt.Utils.GenerateExcelReport;
import com.bezkoder.springjwt.handler.ProcessException;
import com.bezkoder.springjwt.models.CountChartModel;
import com.bezkoder.springjwt.models.CountRequestModel;
import com.bezkoder.springjwt.models.CustomerModel;
import com.bezkoder.springjwt.models.IdRequest;
import com.bezkoder.springjwt.models.SearchStartEndDateModel;
import com.bezkoder.springjwt.models.Vehicle;
import com.bezkoder.springjwt.models.VehicleModel;
import com.bezkoder.springjwt.payload.response.MessageResponse;
import com.bezkoder.springjwt.payload.response.ResponseModel;
import com.bezkoder.springjwt.repository.CustomerRepository;
import com.bezkoder.springjwt.repository.VehicleRepository;

@CrossOrigin(origins = "http://localhost:4200", maxAge = 3600)
@RestController
@RequestMapping("/api/cusAndveh")
public class CusAndVehController {
	@Autowired
	private VehicleRepository vehicleRepo;
	
	@Autowired
	private CustomerRepository customerRepo;
	
//	@Autowired
//	private VehicleYearRepository vYearRepo;
//	
	@Autowired
	private CusAndVehBAO cusAndVehBAO;
	
	@PreAuthorize("hasRole('USER')")
	@PostMapping(value = "/list", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseModel<CustomerModel> getTest (@RequestBody CustomerModel param){
		return cusAndVehBAO.getTest(param);
	}
	@PreAuthorize("hasRole('USER')")
	@PostMapping(value = "/saveCus", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseModel<CustomerModel> saveCus (@RequestBody CustomerModel param) throws ProcessException{
		return cusAndVehBAO.saveCustomer(param);
	}
	@PreAuthorize("hasRole('USER')")
	@PostMapping(value = "/saveVeh", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseModel<VehicleModel> saveVeh (@RequestBody VehicleModel param) throws ProcessException{
		
		return cusAndVehBAO.saveVehicle(param);
	}
	@PreAuthorize("hasRole('USER')")
	@PostMapping(value = "/getByCitizenId", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseModel<CustomerModel> getByCustomerId (@RequestBody IdRequest param) throws ProcessException{
		
		return cusAndVehBAO.getByCitizenId(param);
	}
	@PreAuthorize("hasRole('USER')")
	@PostMapping(value = "/getList", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseModel<List<CustomerModel>> getCusAndVehList() throws ProcessException{
		
		return cusAndVehBAO.getCusAndVehList();
	}
	@PreAuthorize("hasRole('USER')")
	@PostMapping(value = "/getByBetweenAccessDate", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseModel<List<VehicleModel>> getCusAndVehByBetweenAccessDate(@RequestBody SearchStartEndDateModel param) throws ProcessException{
	
		return cusAndVehBAO.getCusAndVehAccessDateBetween(param);
	}
	@PreAuthorize("hasRole('USER')")
	@PostMapping(value = "/getByExpire7Day", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseModel<List<VehicleModel>> getExpire7Day() throws ProcessException{
	
		return cusAndVehBAO.getExpiredIn7Day();
	}
	
	@PreAuthorize("hasRole('USER')")
	@PostMapping(value = "/getByExpire30Day", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseModel<List<VehicleModel>> getExpire30Day() throws ProcessException{
	
		return cusAndVehBAO.getExpiredIn30Day();
	}
	@PreAuthorize("hasRole('USER')")
	@PostMapping(value = "/deleteVehById", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> deleteVehById(@RequestBody IdRequest param) throws ProcessException{
		if(param.getIntId() != null) {
			vehicleRepo.deleteById(param.getIntId());
			return ResponseEntity.ok(new MessageResponse("Delete Vehicle Successful"));
		}
		else {
			return ResponseEntity.badRequest().body(new MessageResponse("Some Data Missing"));
		}
	}
	@PreAuthorize("hasRole('USER')")
	@PostMapping(value = "/deleteCusById", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> deleteCusById(@RequestBody IdRequest param) throws ProcessException{
		if(param.getIntId() != null) {
			customerRepo.deleteById(param.getIntId());
			return ResponseEntity.ok(new MessageResponse("Delete Customer And Relation Vehicle Successful"));
		}
		else {
			return ResponseEntity.badRequest().body(new MessageResponse("Some Data Missing"));
		}
	}
	@PreAuthorize("hasRole('USER')")
	@PostMapping(value = "/generateExcel")
	public ResponseEntity<InputStreamResource> excelCustomersReport(@RequestBody SearchStartEndDateModel param) throws IOException {
		
	    List<Vehicle> vehicles = (List<Vehicle>) vehicleRepo.findByAccessdateBetween(param.getStartDate(),param.getEndDate());
	    String filename = "Customer"+param.getStartDate().toString()+"to"+param.getEndDate().toString()+".xlsx";
	    ByteArrayInputStream in = GenerateExcelReport.vehicleToExcel(vehicles,filename);
	    // return IO ByteArray(in);
	    HttpHeaders headers = new HttpHeaders();
	    // set filename in header
	    headers.add("Content-Disposition", "attachment; filename=Customer.xlsx");
	    return ResponseEntity.ok().headers(headers).contentType(MediaType.parseMediaType("application/vnd.ms-excel"))
	    		.body(new InputStreamResource(in));
	
	}
	@PreAuthorize("hasRole('USER')")
	@PostMapping(value = "/getVehicleList", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseModel<List<VehicleModel>> getVehicleList() throws ProcessException{
		
		return cusAndVehBAO.getVehicleList();
	}
	@PreAuthorize("hasRole('USER')")
	@PostMapping(value = "/getCountByparam", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseModel<CountChartModel> getCountByparam(@RequestBody CountRequestModel param) throws ProcessException{
		
		return cusAndVehBAO.getCountByparam(param);
	}
	@PreAuthorize("hasRole('USER')")
	@PostMapping(value = "/getVehicleListbyEnable", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseModel<List<VehicleModel>> getVehicleListbyEnable() throws ProcessException{
		
		return cusAndVehBAO.getVehicleListbyEnable();
	}
	
}
