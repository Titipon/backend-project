package com.bezkoder.springjwt.Utils;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

public class DateUtils {
    public static final String DATE_FORMAT = "yyyyMMdd";
    public static final String TIME_FORMAT_HHmmss = "HH:mm:ss";
    public static final String DATE_TIME_FORMAT = "yyyyMMddHHmmss";
    public static final String DATE_TIME_FORMAT_UI = "dd/MM/yyyy - HH:mm:ss";
    public static final String DATE_TIME_FORMAT_MYMO = "yyyy-MM-dd hh:mm:ss";
    public static final String OTP_EXPIRE_DATE_TIME_FORMAT = "yyyyMMdd HHmmss";

    public static final String getCurrentDateTimeString() {
	return formatDate(getCurrentDate(), DATE_TIME_FORMAT);
    }

    public static final Date getCurrentDate() {
	Calendar calendar = Calendar.getInstance(Locale.ENGLISH);
	return new Date(calendar.getTime().getTime());
    }

    public static final Date getFromDate(Date date) {
	Calendar calendar = Calendar.getInstance(Locale.ENGLISH);
	calendar.setTime(date);
	calendar.set(Calendar.HOUR_OF_DAY, 0);
	calendar.set(Calendar.MINUTE, 0);
	calendar.set(Calendar.SECOND, 0);
	calendar.set(Calendar.MILLISECOND, 0);
	return calendar.getTime();
    }

    public static final Date getToDate(Date date) {
	Calendar calendar = Calendar.getInstance(Locale.ENGLISH);
	calendar.setTime(date);
	calendar.set(Calendar.HOUR_OF_DAY, 23);
	calendar.set(Calendar.MINUTE, 59);
	calendar.set(Calendar.SECOND, 59);
	calendar.set(Calendar.MILLISECOND, 99);
	return calendar.getTime();
    }

    public static final Long diffDays(Date startDate, Date endDate) {
	long diffDays = 0;
	// in milliseconds
	if (startDate != null && endDate != null) {
	    long diff = endDate.getTime() - startDate.getTime();
	    diffDays = diff / (24 * 60 * 60 * 1000);
	}
	return diffDays;
    }

    // yyyyMMddHHmmss>>Date()
    public static final Date getDateString(String _date) {

	int year = Integer.parseInt(_date.substring(0, 4));
	int month = Integer.parseInt(_date.substring(4,6));
	int day = Integer.parseInt(_date.substring(6, 8));
	int hour = Integer.parseInt(_date.substring(8, 10));
	int minute = Integer.parseInt(_date.substring(10, 12));
	int secound = Integer.parseInt(_date.substring(12, 14));

	Calendar calendar = Calendar.getInstance(Locale.ENGLISH);
	calendar.set(Calendar.YEAR, year);
	calendar.set(Calendar.MONTH, month-1);
	calendar.set(Calendar.DAY_OF_MONTH, day);
	calendar.set(Calendar.HOUR_OF_DAY, hour);
	calendar.set(Calendar.MINUTE, minute);
	calendar.set(Calendar.SECOND, secound);

	return calendar.getTime();
    }

    public static final Date getDateString(String dateString, String dateFormat) {
	SimpleDateFormat sdf = new SimpleDateFormat(dateFormat, Locale.ENGLISH);

	try {
	    return sdf.parse(dateString);
	} catch (ParseException e) {
	    return null;
	}

    }

    public static String formatDate(Date date, String format) {
	SimpleDateFormat sdf = new SimpleDateFormat(format, Locale.ENGLISH);
	if (date != null)
	    return sdf.format(date);
	else
	    return null;
    }

    public static Date getExpireDate(Integer times) {
	Calendar curdate = Calendar.getInstance();
	Date dateCurrent = getCurrentDate();
	curdate.setTime(dateCurrent);
	curdate.add(Calendar.MINUTE, times);
	Date date = curdate.getTime();
	return date;
    }

    public static Date getExpireDate(Date dateCurrent, Integer times) {
	Calendar curdate = Calendar.getInstance();
	curdate.setTime(dateCurrent);
	curdate.add(Calendar.MINUTE, times);
	Date date = curdate.getTime();
	return date;
    }

    public static boolean validationExpireDateTimeStr(String dateTimeStr, String dateFormat) {
	try {
	    if (dateTimeStr == null || dateTimeStr.isEmpty() || dateFormat == null || dateFormat.isEmpty())
		return false;

	    Date currentDate = getCurrentDate();
	    Date compareDate = getDateString(dateTimeStr, dateFormat);
	    if (compareDate != null)
		return currentDate.after(compareDate);
	    else
		return false;
	} catch (Exception e) {
	    return false;
	}
    }
    public static String getRestDate(Date date){
		SimpleDateFormat dtf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
		Date d = getCurrentDate();
		dtf.format(date);
		dtf.format(d);
		long diff = Math.abs(d.getTime() - date.getTime());
		long restDiff = diff / (24 * 60 * 60 * 1000);
		restDiff = restDiff + 1;
		String restDay = Long.toString(restDiff)+" วัน";
		return restDay;	
	}
}
