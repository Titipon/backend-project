package com.bezkoder.springjwt.Utils;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.DataFormat;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.bezkoder.springjwt.models.SearchStartEndDateModel;
import com.bezkoder.springjwt.models.Vehicle;


public class GenerateExcelReport {
	public static ByteArrayInputStream vehicleToExcel(List<Vehicle> vehicles,String filename) throws IOException {
	
		String[] COLUMNs = { "AccessDate","CustomerId", "CitizenId", "CustomerName", "CustomerTelNo", "VehicleId", "VehicleReg","VehicleProvince",
				"VehicleChassisNo","VehicleStartDate","VehicleExpiredDate","VehicleServiceType","VehiclePolicyNo","VehicleInsuranceType","VehicleInsuranceCompany",
				"VehicleActFee","ServiceFee","VehicleTax","PremiumFee"};
		try (Workbook workbook = new XSSFWorkbook(); ByteArrayOutputStream out = new ByteArrayOutputStream();FileOutputStream file = new FileOutputStream(new File("D:\\"+filename))) {
			Sheet sheet = workbook.createSheet("Users");
			
			Font headerFont = workbook.createFont();
			headerFont.setBold(true);
			headerFont.setColor(IndexedColors.BLUE.getIndex());

			CellStyle headerCellStyle = workbook.createCellStyle();
			headerCellStyle.setFont(headerFont);
			
			CellStyle cellStyle = workbook.createCellStyle();
			CreationHelper creationHelper = workbook.getCreationHelper();
			cellStyle.setDataFormat(creationHelper.createDataFormat().getFormat("dd-MM-yyyy HH:mm:ss"));

			CellStyle cellStylenumber = workbook.createCellStyle();
			DataFormat dataformat = workbook.createDataFormat();
			cellStylenumber.setDataFormat(dataformat.getFormat("#,##0.00"));
			// Header Row
			Row headerRow = sheet.createRow(0);

			// Table Header
			for (int col = 0; col < COLUMNs.length; col++) {
				Cell cell = headerRow.createCell(col);
				cell.setCellValue(COLUMNs[col]);
				cell.setCellStyle(headerCellStyle);
			}

			int rowIdx = 1;
			for (Vehicle vehicle : vehicles) {
				Row row = sheet.createRow(rowIdx++);
				
				
				row.createCell(0).setCellStyle(cellStyle);
				row.createCell(0).setCellValue(vehicle.getAccessdate());
				row.createCell(1).setCellValue(vehicle.getCustomer().getCusId());
				row.createCell(2).setCellValue(vehicle.getCustomer().getCitizenId());
				row.createCell(3).setCellValue(vehicle.getCustomer().getPrefixname().concat(vehicle.getCustomer().getName()).concat(" ").concat(vehicle.getCustomer().getSurname()));
				row.createCell(4).setCellValue(vehicle.getCustomer().getTelno());
				row.createCell(5).setCellValue(vehicle.getVehicleId());
				row.createCell(6).setCellValue(vehicle.getVehicleReg());
				row.createCell(7).setCellValue(vehicle.getProvince());
				row.createCell(8).setCellValue(vehicle.getChassisNo());
				row.createCell(9).setCellStyle(cellStyle);
				row.createCell(9).setCellValue(vehicle.getStartdate());
				row.createCell(10).setCellStyle(cellStyle);
				row.createCell(10).setCellValue(vehicle.getExpiredate());
				row.createCell(11).setCellValue(vehicle.getServiceType());
				row.createCell(12).setCellValue(vehicle.getPolicyNo());
				row.createCell(13).setCellValue(vehicle.getInsuranceType());
				row.createCell(14).setCellValue(vehicle.getInsuranceCompany());
				row.createCell(15).setCellStyle(cellStylenumber);
				row.createCell(15).setCellValue(vehicle.getVehicleACTfee());
				row.createCell(16).setCellStyle(cellStylenumber);
				row.createCell(16).setCellValue(vehicle.getServicefee());
				row.createCell(17).setCellStyle(cellStylenumber);
				row.createCell(17).setCellValue(vehicle.getVehicletax());
				row.createCell(18).setCellStyle(cellStylenumber);
				row.createCell(18).setCellValue(vehicle.getPremiumfee());
				
				
			}
			
			//Auto-size all the above columns
			sheet.autoSizeColumn(0);
			sheet.autoSizeColumn(1);
			sheet.autoSizeColumn(2);
			sheet.autoSizeColumn(3);
			sheet.autoSizeColumn(4);
			sheet.autoSizeColumn(5);
			sheet.autoSizeColumn(6);
			sheet.autoSizeColumn(7);
			sheet.autoSizeColumn(8);
			sheet.autoSizeColumn(9);
			sheet.autoSizeColumn(10);
			sheet.autoSizeColumn(11);
			sheet.autoSizeColumn(12);
			sheet.autoSizeColumn(13);
			sheet.autoSizeColumn(14);
			sheet.autoSizeColumn(15);
			sheet.autoSizeColumn(16);
			sheet.autoSizeColumn(17);
			sheet.autoSizeColumn(18);

			workbook.write(file);
			file.close();
			workbook.close();
			return new ByteArrayInputStream(out.toByteArray());
		}

	}
}
