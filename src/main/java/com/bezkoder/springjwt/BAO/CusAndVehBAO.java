package com.bezkoder.springjwt.BAO;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bezkoder.springjwt.handler.ProcessException;
import com.bezkoder.springjwt.handler.ServiceResult;
import com.bezkoder.springjwt.models.CountChartModel;
import com.bezkoder.springjwt.models.CountRequestModel;
import com.bezkoder.springjwt.models.Customer;
import com.bezkoder.springjwt.models.CustomerModel;
import com.bezkoder.springjwt.models.IdRequest;
import com.bezkoder.springjwt.models.SearchStartEndDateModel;
import com.bezkoder.springjwt.models.Vehicle;
import com.bezkoder.springjwt.models.VehicleModel;
import com.bezkoder.springjwt.payload.response.ResponseModel;
import com.bezkoder.springjwt.security.services.CusAndVehService;

@Component
public class CusAndVehBAO implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Autowired
	private CusAndVehService cusAndvehService;
	
	
	public ResponseModel<CustomerModel> getTest(CustomerModel param){
		if (param.getCitizenId()==null) {
			
		}
		//save
		ResponseModel<CustomerModel> cusModel = new ResponseModel<CustomerModel>();
		cusModel.setData(param);
		cusModel.setCode("0001");
		cusModel.setDesc("TestCode");
		return cusModel;
	}
	public ResponseModel<CustomerModel> saveCustomer(CustomerModel param) throws ProcessException{
		if(param.getCitizenId() == null || param.getName() == null || 
				param.getPrefixname() == null || param.getSurname() == null || param.getTelno() == null) {
				throw new ProcessException("Error some data is missing");
		}
		Customer copyparam = new Customer();
		BeanUtils.copyProperties(param, copyparam);
		ServiceResult<Customer> serviceResult = cusAndvehService.saveCustomer(copyparam);
		if(serviceResult.isSuccess()) {
			ResponseModel<CustomerModel> responseModel = new ResponseModel<CustomerModel>();
			Customer result = serviceResult.getData();
			CustomerModel model = new CustomerModel(result);
			responseModel.setData(model);
			responseModel.setCode("0001");
			responseModel.setDesc("Save Success");
			return responseModel;
		}
		else {
			throw new ProcessException(serviceResult.getMessageCode());
		}
	}
	public ResponseModel<VehicleModel> saveVehicle(VehicleModel param) throws ProcessException{
		if(param.getReceivetype() == null ||  param.getAccessdate() == null || param.getChassisNo() == null || param.getCreateby() == null || param.getCreatedate() == null||
				param.getCusid() == null || param.getExpiredate() == null || param.getServiceType() == null || param.getStartdate() == null ||
				param.getVehicleReg() == null || param.getProvince() == null || param.getVehiclebrand() == null || param.getVehiclemodel() == null) {
				throw new ProcessException("Error some data is missing");
		}
		Customer customer = new Customer();
		Vehicle copyparam = new Vehicle();
		customer.setCusId(param.getCusid());
		copyparam.setCustomer(customer);
		//copy
		BeanUtils.copyProperties(param, copyparam);
		copyparam.setVehicleType(param.getVehicletype());
		ServiceResult<Vehicle> serviceResult = cusAndvehService.saveVehicle(copyparam);
		if(serviceResult.isSuccess()) {
			ResponseModel<VehicleModel> responseModel = new ResponseModel<VehicleModel>();
			Vehicle result = serviceResult.getData();
			VehicleModel model = new VehicleModel(result);
			responseModel.setData(model);
			responseModel.setCode("0001");
			responseModel.setDesc("Save Success");
			return responseModel;
		}
		else {
			throw new ProcessException(serviceResult.getMessageCode());
		}
//		ResponseModel<CustomerModel> cusModel = new ResponseModel<CustomerModel>();
//		cusModel.setData(param);
//		cusModel.setCode("0001");
//		cusModel.setDesc("TestCode");
//		return cusModel;
	}
	public ResponseModel<List<CustomerModel>> getCusAndVehList() throws ProcessException{
		ServiceResult<List<Customer>> serviceResult = cusAndvehService.getCusAndVehList();
		if (serviceResult.isSuccess()) {
			List<Customer> searchResult = serviceResult.getData();
			List<CustomerModel> result = new ArrayList<CustomerModel>();
			for (Customer obj : searchResult) {
				CustomerModel model = new CustomerModel(obj);
				result.add(model);
			}
			ResponseModel<List<CustomerModel>> responseModel = new ResponseModel<List<CustomerModel>>();
			responseModel.setData(result);
			responseModel.setCode("0002");
			responseModel.setDesc("Get List Success");
			return responseModel;
		}
		else {
			throw new ProcessException(serviceResult.getMessageCode());
		}
		
	}
	public ResponseModel<List<VehicleModel>> getCusAndVehAccessDateBetween(SearchStartEndDateModel param) throws ProcessException{
		if(param.getStartDate() == null || param.getEndDate() == null) {
				throw new ProcessException("Error some data is missing");
		}
		ServiceResult<List<Vehicle>> serviceResult = cusAndvehService.getCusAndVehByAccessdateBetween(param);
		if (serviceResult.isSuccess()) {
			List<Vehicle> searchResult = serviceResult.getData();
			List<VehicleModel> result = new ArrayList<VehicleModel>();
			searchResult.remove(null);
//			BeanUtils.copyProperties(searchResult, result);
			for (Vehicle obj : searchResult) {
				if (obj != null) {
					VehicleModel model = new VehicleModel(obj);
					result.add(model);
				}
			}
			ResponseModel<List<VehicleModel>> responseModel = new ResponseModel<List<VehicleModel>>();
			responseModel.setData(result);
			responseModel.setCode("0002");
			responseModel.setDesc("Get List Success");
			return responseModel;
		}
		else {
			throw new ProcessException(serviceResult.getMessageCode());
		}
	}
//	public ResponseModel<List<CustomerModel>> getCusAndVehAccessDateBetween(SearchStartEndDateModel param) throws ProcessException{
//		if(param.getStartDate() == null || param.getEndDate() == null) {
//				throw new ProcessException("Error some data is missing");
//		}
//		ServiceResult<List<Customer>> serviceResult = cusAndvehService.getCusAndVehByAccessdateBetween(param);
//		if (serviceResult.isSuccess()) {
//			List<Customer> searchResult = serviceResult.getData();
//			List<CustomerModel> result = new ArrayList<CustomerModel>();
//			searchResult.remove(null);
////			BeanUtils.copyProperties(searchResult, result);
//			for (Customer obj : searchResult) {
//				if (obj != null) {
//					CustomerModel model = new CustomerModel(obj);
//					result.add(model);
//				}
//			}
//			ResponseModel<List<CustomerModel>> responseModel = new ResponseModel<List<CustomerModel>>();
//			responseModel.setData(result);
//			responseModel.setCode("0002");
//			responseModel.setDesc("Get List Success");
//			return responseModel;
//		}
//		else {
//			throw new ProcessException(serviceResult.getMessageCode());
//		}
//	}
	public ResponseModel<List<VehicleModel>> getExpiredIn7Day() throws ProcessException{
		ServiceResult<List<Vehicle>> serviceResult = cusAndvehService.getCusAndVehByExpiredate7Day();
		if (serviceResult.isSuccess()) {
			List<Vehicle> searchResult = serviceResult.getData();
			List<VehicleModel> result = new ArrayList<VehicleModel>();
			for (Vehicle obj : searchResult) {
				VehicleModel model = new VehicleModel(obj);
				result.add(model);
			}
			ResponseModel<List<VehicleModel>> responseModel = new ResponseModel<List<VehicleModel>>();
			responseModel.setData(result);
			responseModel.setCode("0002");
			responseModel.setDesc("Get List Success");
			return responseModel;
		}
		else {
			throw new ProcessException(serviceResult.getMessageCode());
		}
		
	}
	public ResponseModel<List<VehicleModel>> getExpiredIn30Day() throws ProcessException{
		ServiceResult<List<Vehicle>> serviceResult = cusAndvehService.getCusAndVehByExpiredate30Day();
		if (serviceResult.isSuccess()) {
			List<Vehicle> searchResult = serviceResult.getData();
			List<VehicleModel> result = new ArrayList<VehicleModel>();
			for (Vehicle obj : searchResult) {
				VehicleModel model = new VehicleModel(obj);
				result.add(model);
			}
			ResponseModel<List<VehicleModel>> responseModel = new ResponseModel<List<VehicleModel>>();
			responseModel.setData(result);
			responseModel.setCode("0002");
			responseModel.setDesc("Get List Success");
			return responseModel;
		}
		else {
			throw new ProcessException(serviceResult.getMessageCode());
		}
		
	}
	public ResponseModel<CustomerModel> getByCitizenId(IdRequest param) throws ProcessException {
		if(param.getCitizenid() == null ) {
			throw new ProcessException("Error some data is missing");
	    }
		
		ServiceResult<Customer> serviceResult = cusAndvehService.getCustomerByCusId(param);
		if (serviceResult.isSuccess()) {
			Customer searchResult = serviceResult.getData();
			CustomerModel result = new CustomerModel();
			BeanUtils.copyProperties(searchResult,result);
			ResponseModel<CustomerModel> responseModel = new ResponseModel<CustomerModel>();
			responseModel.setData(result);
			responseModel.setCode("0002");
			responseModel.setDesc("Get List Success");
			return responseModel;
		}
		else {
			throw new ProcessException(serviceResult.getMessageCode());
		}
	}
//	public ResponseModel<VehicleDetailModel> saveVehicleDetail(VehicleDetailModel param) throws ProcessException{
//		if(param.getVd_brand() == null || param.getVd_spec() == null) {
//				throw new ProcessException("Error some data is missing");
//		}
//		VehicleDetail saveVehDetail = new VehicleDetail();
//		BeanUtils.copyProperties(param, saveVehDetail);
//		ServiceResult<VehicleDetail> serviceResult = cusAndvehService.saveVehDetail(saveVehDetail);
//		if(serviceResult.isSuccess()) {
//			ResponseModel<VehicleDetailModel> responseModel = new ResponseModel<VehicleDetailModel>();
//			VehicleDetail result = serviceResult.getData();
//			VehicleDetailModel model = new VehicleDetailModel(result);
//			responseModel.setData(model);
//			responseModel.setCode("0001");
//			responseModel.setDesc("Save Success");
//			return responseModel;
//		}
//		else {
//			throw new ProcessException(serviceResult.getMessageCode());
//		}
//	}
//	public ResponseModel<List<VehicleDetailModel>> getVehicleDetailList() throws ProcessException{
//		ServiceResult<List<VehicleDetail>> serviceResult = cusAndvehService.getAllVehDetail();
//		if (serviceResult.isSuccess()) {
//			List<VehicleDetail> searchResult = serviceResult.getData();
//			List<VehicleDetailModel> result = new ArrayList<VehicleDetailModel>();
//			for (VehicleDetail obj : searchResult) {
//				VehicleDetailModel model = new VehicleDetailModel(obj);
//				result.add(model);
//			}
//			ResponseModel<List<VehicleDetailModel>> responseModel = new ResponseModel<List<VehicleDetailModel>>();
//			responseModel.setData(result);
//			responseModel.setCode("0002");
//			responseModel.setDesc("Get List Success");
//			return responseModel;
//		}
//		else {
//			throw new ProcessException(serviceResult.getMessageCode());
//		}
//		
//	}
	public ResponseModel<List<VehicleModel>> getVehicleList()  throws ProcessException{
		ServiceResult<List<Vehicle>> serviceResult = cusAndvehService.getVehicleList();
		if (serviceResult.isSuccess()) {
			List<Vehicle> searchResult = serviceResult.getData();
			List<VehicleModel> result = new ArrayList<VehicleModel>();
			for (Vehicle obj : searchResult) {
				VehicleModel model = new VehicleModel(obj);
				result.add(model);
			}
			ResponseModel<List<VehicleModel>> responseModel = new ResponseModel<List<VehicleModel>>();
			responseModel.setData(result);
			responseModel.setCode("0002");
			responseModel.setDesc("Get List Success");
			return responseModel;
		}
		else {
			throw new ProcessException(serviceResult.getMessageCode());
		}
	}
//	public ResponseModel<SpecDetailModel> saveSpecDetail(SpecDetailModel param)  throws ProcessException{
//		if(param.getVehiclespec() == null ) {
//			throw new ProcessException("Error some data is missing");
//	}
//	SpecDetail saveSpecDetail = new SpecDetail();
//	BeanUtils.copyProperties(param, saveSpecDetail);
//	ServiceResult<SpecDetail> serviceResult = cusAndvehService.saveSpecDetail(saveSpecDetail);
//	if(serviceResult.isSuccess()) {
//		ResponseModel<SpecDetailModel> responseModel = new ResponseModel<SpecDetailModel>();
//		SpecDetail result = serviceResult.getData();
//		SpecDetailModel model = new SpecDetailModel(result);
//		responseModel.setData(model);
//		responseModel.setCode("0001");
//		responseModel.setDesc("Save Success");
//		return responseModel;
//	}
//	else {
//		throw new ProcessException(serviceResult.getMessageCode());
//		}
//	}
//	public ResponseModel<List<SpecDetailModel>> getSpecList()  throws ProcessException{
//		ServiceResult<List<SpecDetail>> serviceResult = cusAndvehService.getSpecList();
//		if (serviceResult.isSuccess()) {
//			List<SpecDetail> searchResult = serviceResult.getData();
//			List<SpecDetailModel> result = new ArrayList<SpecDetailModel>();
//			for (SpecDetail obj : searchResult) {
//				SpecDetailModel model = new SpecDetailModel(obj);
//				result.add(model);
//			}
//			ResponseModel<List<SpecDetailModel>> responseModel = new ResponseModel<List<SpecDetailModel>>();
//			responseModel.setData(result);
//			responseModel.setCode("0002");
//			responseModel.setDesc("Get List Success");
//			return responseModel;
//		}
//		else {
//			throw new ProcessException(serviceResult.getMessageCode());
//		}
//	}
//	public ResponseModel<List<VehicleYearModel>> getVehicleYearList() throws ProcessException{
//		ServiceResult<List<VehicleYear>> serviceResult = cusAndvehService.getVehicleYearList();
//		if (serviceResult.isSuccess()) {
//			List<VehicleYear> searchResult = serviceResult.getData();
//			List<VehicleYearModel> result = new ArrayList<VehicleYearModel>();
//			for (VehicleYear obj : searchResult) {
//				VehicleYearModel model = new VehicleYearModel(obj);
//				result.add(model);
//			}
//			ResponseModel<List<VehicleYearModel>> responseModel = new ResponseModel<List<VehicleYearModel>>();
//			responseModel.setData(result);
//			responseModel.setCode("0002");
//			responseModel.setDesc("Get List Success");
//			return responseModel;
//		}
//		else {
//			throw new ProcessException(serviceResult.getMessageCode());
//		}
//	}
//	public ResponseModel<VehicleYearModel> saveYear(VehicleYearModel param)  throws ProcessException{
//		if(param.getVehicleyear() == null ) {
//			throw new ProcessException("Error some data is missing");
//	}
//	VehicleYear savevehicleYear = new VehicleYear();
//	BeanUtils.copyProperties(param, savevehicleYear);
//	ServiceResult<VehicleYear> serviceResult = cusAndvehService.saveYear(savevehicleYear);
//	if(serviceResult.isSuccess()) {
//		ResponseModel<VehicleYearModel> responseModel = new ResponseModel<VehicleYearModel>();
//		VehicleYear result = serviceResult.getData();
//		VehicleYearModel model = new VehicleYearModel(result);
//		responseModel.setData(model);
//		responseModel.setCode("0001");
//		responseModel.setDesc("Save Success");
//		return responseModel;
//	}
//	else {
//		throw new ProcessException(serviceResult.getMessageCode());
//		}
//	}
	public ResponseModel<CountChartModel> getCountByparam(CountRequestModel param) throws ProcessException {
		if(param.getParam() == null || param.getRequestname() == null ) {
			throw new ProcessException("Error some data is missing");
	}	
		ServiceResult<CountChartModel> servicecount = new ServiceResult<CountChartModel>();
		if(param.isStatus() == true) {
			servicecount = cusAndvehService.getCountByparam(param);
		}
		else{
			servicecount = cusAndvehService.getCountByTaxInsur(param);
		}
		if(servicecount.isSuccess()) {
			ResponseModel<CountChartModel> count = new ResponseModel<CountChartModel>();
			CountChartModel rescount = servicecount.getData();
			count.setCode("01");
			count.setData(rescount);
			count.setDesc("GetCountOK");
			return count;
			
		}
		
		else {
			throw new ProcessException(servicecount.getMessageCode());
		}
		
	}
	public ResponseModel<List<VehicleModel>> getVehicleListbyEnable()  throws ProcessException{
		ServiceResult<List<Vehicle>> serviceResult = cusAndvehService.getVehicleListbyEnable();
		if (serviceResult.isSuccess()) {
			List<Vehicle> searchResult = serviceResult.getData();
			List<VehicleModel> result = new ArrayList<VehicleModel>();
			for (Vehicle obj : searchResult) {
				VehicleModel model = new VehicleModel(obj);
				result.add(model);
			}
			ResponseModel<List<VehicleModel>> responseModel = new ResponseModel<List<VehicleModel>>();
			responseModel.setData(result);
			responseModel.setCode("0002");
			responseModel.setDesc("Get List Success");
			return responseModel;
		}
		else {
			throw new ProcessException(serviceResult.getMessageCode());
		}
	}
}
