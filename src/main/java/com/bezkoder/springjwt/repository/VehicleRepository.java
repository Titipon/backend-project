package com.bezkoder.springjwt.repository;

import java.sql.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.CrossOrigin;

import com.bezkoder.springjwt.models.Customer;
import com.bezkoder.springjwt.models.Vehicle;

@CrossOrigin(origins = "http://localhost:4200")
@Repository
public interface VehicleRepository extends PagingAndSortingRepository<Vehicle, Integer>,JpaSpecificationExecutor<Vehicle>{
	
	public List<Vehicle> getByCustomer(Customer param);

	@Query(value = "select * from Customer c join Vehicle v on c.cus_id = v.cus_id where v.accessdate between :startDate and :endDate and v.status = 'enable' order by v.accessdate asc",nativeQuery = true)
	public List<Vehicle> findByAccessdateBetween(Date startDate,Date endDate);
	
	@Query(value = "select * from Customer c join Vehicle v on c.cus_id = v.cus_id where v.expire_date <= now()+interval 7 DAY and v.expire_date > now() and v.status = 'enable' order by v.expire_date asc",nativeQuery = true)
	public List<Vehicle> findByExpireddate7Day();
		
	@Query(value = "select * from Customer c join Vehicle v on c.cus_id = v.cus_id where v.expire_date <= now()+interval 30 DAY and v.expire_date > now() and v.status = 'enable' order by v.expire_date asc",nativeQuery = true)
	public List<Vehicle> findByExpireddate30Day();
	
	@Query(value = "select * from Customer c join Vehicle v on c.cus_id = v.cus_id where v.status = 'enable' order by v.accessdate asc",nativeQuery = true)
	public List<Vehicle> findAllbystatus();
	
	
	public long countByVehicleType(String param);
	
	public long countByServiceType(String param);
	
	
	
}
