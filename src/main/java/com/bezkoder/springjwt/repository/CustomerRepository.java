package com.bezkoder.springjwt.repository;

import java.sql.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.CrossOrigin;

import com.bezkoder.springjwt.models.Customer;

@CrossOrigin(origins = "http://localhost:4200")
@Repository
public interface CustomerRepository extends PagingAndSortingRepository<Customer, Integer>, JpaSpecificationExecutor<Customer> {

	public Customer findBycitizenId(String citizenId);
	
	
	@Query(value = "select * from Customer c join Vehicle v on c.cus_id = v.cus_id where v.accessdate between :startDate and :endDate",nativeQuery = true)
	public List<Customer> findByAccessdateBetween(Date startDate,Date endDate);
	
	@Query(value = "select * from Customer c join Vehicle v on c.cus_id = v.cus_id where v.expire_date >= now()+interval 7 DAY and v.expire_date > now();",nativeQuery = true)
	public List<Customer> findByExpire_date7Day();
	
	@Query(value = "select * from Customer c join Vehicle v on c.cus_id = v.cus_id where c.status = 'enable' and v.status = 'enable';",nativeQuery = true)
	public List<Customer> findAllbyStatus();
}
